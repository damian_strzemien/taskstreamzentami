import java.util.Objects;

public class Ent implements Comparable<Ent> {


    private String gatunkek;
    private TypDrzewa typDrzewa;
    private int wysokosc;
    private int wiek;
    private String imie;

    @Override
    public String toString() {
        return "Ent{" +
                "gatunkek='" + gatunkek + '\'' +
                ", typDrzewa=" + typDrzewa +
                ", wysokosc=" + wysokosc +
                ", wiek=" + wiek +
                ", imie='" + imie + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ent ent = (Ent) o;

        if (wysokosc != ent.wysokosc) return false;
        if (wiek != ent.wiek) return false;
        if (gatunkek != null ? !gatunkek.equals(ent.gatunkek) : ent.gatunkek != null) return false;
        if (typDrzewa != ent.typDrzewa) return false;
        return imie != null ? imie.equals(ent.imie) : ent.imie == null;
    }

    @Override
    public int hashCode() {
        int result = gatunkek != null ? gatunkek.hashCode() : 0;
        result = 31 * result + (typDrzewa != null ? typDrzewa.hashCode() : 0);
        result = 31 * result + wysokosc;
        result = 31 * result + wiek;
        result = 31 * result + (imie != null ? imie.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(Ent ent) {
        int ret = typDrzewa.compareTo(ent.typDrzewa);

        if (ret == 0) {
            ret = gatunkek.compareTo(ent.gatunkek);
        }
        if (ret == 0) {
            ret = this.wysokosc - ent.wysokosc;
        }
        if (ret == 0) {
            ret = this.wiek - ent.wiek;
        }
        if (ret == 0) {
            ret = imie.compareTo(ent.imie);
        }
        return ret;
    }

    public int getWysokosc() {
        return wysokosc;
    }

    public void setWysokosc(int wysokosc) {
        this.wysokosc = wysokosc;
    }

    public int getWiek() {
        return wiek;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }
}